<?php

if (!function_exists('______dmInternalShutdown')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmInternalShutdown()
    {
        global $______output;
        $stdout = ob_get_contents();
        if (!$stdout && isset($______output)) {
            $stdout = $______output;
        }
        $sendedHeaders = [];
        $setCookie = [];
        foreach (headers_list() as $header) {
            $dataHeader = explode(':', $header);
            $headerName = array_shift($dataHeader);
            if (!isset($sendedHeaders[$headerName])) {
                $sendedHeaders[$headerName] = [];
            }
            $sendedHeaders[$headerName][] = trim(implode(':', $dataHeader));
        }
        if (isset($sendedHeaders['Set-Cookie'])) {
            foreach ($sendedHeaders['Set-Cookie'] as $cookieKey => $cookieValueData) {
                $parsedCookie = [];
                parse_str(strtr($cookieValueData, ['&' => '%26', '+' => '%2B', ';' => '&']), $parsedCookie);
                $cookieName = array_keys($parsedCookie)[0];
                $cookieValue = array_values($parsedCookie)[0];
                unset($parsedCookie[$cookieName]);
                $setCookie[$cookieKey] = array_merge([
                    'name' => $cookieName,
                    'value' => $cookieValue,
                ], $parsedCookie);
            }
        }
        
        /** @noinspection PhpComposerExtensionStubsInspection */
        ______dm('shutdown',
            [
                'system' =>
                    array_merge(______dmStartAppInfo()['system'],
                        [
                            'headers' => $sendedHeaders,
                            'set_cookie' => $setCookie,
                            'http_response_code' => http_response_code(),
                            'ob_get_contents' => $stdout,
                            'ob_get_contents_json' => json_decode($stdout, true),
                        ]),
            ]);
        
        ______dmSendFromPull();
    }
}

if (!function_exists('pitch')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function pitch($callable)
    {
        $monitor = new class () {
            /**
             * @var array
             */
            public $collection;
            
            public function __construct()
            {
                $this->collection = [];
            }
            
            public function add(...$vars)
            {
                foreach ($vars as $var) {
                    $this->collection[] = $var;
                }
            }
        };
        try {
            return $callable($monitor);
        } catch (\Throwable $throwable) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $r = new ReflectionFunction($callable);
            if (function_exists('dd')) {
                dd((string)$throwable, $r->getStaticVariables(), $monitor->collection);
            } else {
                var_dump((string)$throwable, $r->getStaticVariables(), $monitor->collection);
                die();
            }
        }
    }
}

if (!function_exists('______dmStartAppInfo')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmStartAppInfo(): array
    {
        $input = file_get_contents('php://input');
        return [
            'system' => [
                'server' => $_SERVER ?? null,
                'request' => $_REQUEST ?? null,
                'get' => $_GET ?? null,
                'cookie' => $_COOKIE ?? null,
                'post' => $_POST ?? null,
                'session' => $_SESSION ?? null,
                'php_input' => $input,
                'php_input_json' => json_decode($input, true),
            ],
        ];
    }
}

if (!function_exists('______dmPullChunk')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmPullChunk(int $chunk)
    {
        global $______dmPullChunk;
        
        if (!isset($______dmPullChunk)) {
            $______dmPullChunk = $chunk;
        }
    }
}

if (!function_exists('______setDmDsn')) {
    function ______setDmDsn($dsn)
    {
        global $______pull;
        
        if (!isset($______pull)) {
            $______pull = [];
        }
        
        global $______dsn;
        if ($______dsn) {
            return;
        }
        $______dsn = $dsn;
        
        function shutdown()
        {
            ______dmInternalShutdown();
        }
        
        register_shutdown_function('shutdown');
        ob_start();
    }
}

if (!function_exists('______dmRecursiveScan')) {
    function ______dmRecursiveScan($data, int $maxStrLen = 4096, int $maxDepth = 10, int $depth = 0)
    {
        $depth++;
        if (is_object($data)) {
            $props = [];
            foreach (______dmGetProperties($data) as $property) {
                if ($depth <= $maxDepth) {
                    $propValue = ______dmRecursiveScan($property->getValue($data), $maxStrLen, $maxDepth, $depth);
                } else {
                    $propValue = "...[truncated by level depth max $maxDepth]";
                }
                $props[$property->getName()] = $propValue;
                /*  $props[$property->getName()] = [
                      '__is_public__' => $property->isPublic(),
                      '__is_protected__' => $property->isProtected(),
                      '__is_private__' => $property->isPrivate(),
                      '__is_static__' => $property->isStatic(),
                      '__type__' => gettype($property->getValue($data)),
                      '__value__' => $propValue,
                  ];*/
            }
            $result = [
                '__object__' => get_class($data),
                '__properties__' => $props,
            ];
        } elseif (is_iterable($data)) {
            $result = [];
            if (is_array($data) && array_key_exists('_____dmMaxDepthOverride', $data) && $depth < 3) {
                $dmMaxDepthOverride = $data['_____dmMaxDepthOverride'];
                unset($data['_____dmMaxDepthOverride']);
            } else {
                $dmMaxDepthOverride = null;
            }
            foreach ($data as $key => $value) {
                $overMaxDepth = $maxDepth;
                if (is_array($data) && $dmMaxDepthOverride !== null && $key === 'data' && $depth < 3) {
                    $overMaxDepth = $dmMaxDepthOverride;
                }
                if ($depth <= $maxDepth) {
                    $result[$key] = ______dmRecursiveScan($value, $maxStrLen, $overMaxDepth, $depth);
                } else {
                    $result[$key] = "...[truncated by level depth max $maxDepth]";
                }
            }
            
        } elseif (is_resource($data) || (is_callable($data) && !is_string($data))) {
            $result = ['__type__' => gettype($data)];
        } elseif (!mb_detect_encoding($data) && preg_match('~[^\x20-\x7E\t\r\n]~', $data) > 0) {
            $result = ['__type__' => 'binary string'];
        } else {
            $len = mb_strlen($data);
            $result = (is_string($data) && $len > $maxStrLen) ? (mb_substr($data, 0, $maxStrLen) . "...[truncated $maxStrLen allowed from $len symbols]") : $data;
        }
        
        return $result;
    }
    
    
}
if (!function_exists('______dmGetProperties')) {
    function ______dmGetProperties($object): array
    {
        $properties = [];
        $rc = new ReflectionObject($object);
        do {
            $rp = [];
            foreach ($rc->getProperties() as $p) {
                $p->setAccessible(true);
                $rp[] = $p;
            }
            $properties = array_merge($rp, $properties);
        } while ($rc = $rc->getParentClass());
        return $properties;
    }
}

if (!function_exists('dm')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function dm($data, string $type = 'custom')
    {
        $data = array_merge(['data' => $data], ______dmStartAppInfo());
        ______dm($type, $data, 4);
    }
}

if (!function_exists('______dmErrorHandler')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmErrorHandler($message, $category, string $baseHttpException = HttpException::class)
    {
        if (!is_a($message, $baseHttpException) || ($message->getCode() >= 500)) {
            if ($message instanceof Throwable) {
                $stacktrace = preg_split('/(\n)?\#(\d+)(\s)?/i', $message->getTraceAsString(), null, PREG_SPLIT_NO_EMPTY) ?: [];
                $stacktraceKeys = [];
                
                foreach ($stacktrace as $key => $stacktraceItem) {
                    $stacktraceKeys["#$key"] = $stacktraceItem;
                }
                
                $data = array_merge([
                    'error' => [
                        '__object__' => get_class($message),
                        '__properties__' => [
                            'message' => $message->getMessage(),
                            'code' => $message->getCode(),
                            'file' => $message->getFile(),
                            'line' => $message->getLine(),
                            'stacktrace' => $stacktraceKeys,
                        ],
                    ],
                ], ______dmStartAppInfo());
            } else {
                $data = array_merge(['error' => $message], ______dmStartAppInfo());
            }
            ______dm('app.error.' . $category, $data);
        }
    }
}

if (!function_exists('______dmDbQuery')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmDbQuery($query)
    {
        $exp = explode(' ', $query);
        if (count($exp)) {
            $category = strtolower($exp[0]);
            $category = trim(preg_replace('/[^A-Z]/i', '', $category));
        } else {
            $category = '[undefined]';
        }
        
        $data = array_merge(['query' => $query], ______dmStartAppInfo());
        ______dm('app.db.query.' . $category, $data);
    }
}

if (!function_exists('______dm')) {
    function ______dm(string $type, $data, int $maxDepth = 10)
    {
        global $______pull;
        global $______dmPullChunk;
        try {
            global $_______requestId;
            if (!$_______requestId) {
                $_______requestId = md5(random_bytes(8));
            }
            $baseData = ['requestId' => $_______requestId, 'mkTime' => microtime(true), 'type' => $type, '_____dmMaxDepthOverride' => $maxDepth];
            $data = array_merge($baseData, $data);
            $______pull[] = $data;
            
            if (isset($______dmPullChunk) && $______dmPullChunk && count($______pull) >= $______dmPullChunk) {
                ______dmSendFromPull();
            } elseif (count($______pull) >= 20) {
                ______dmSendFromPull();
            }
            
        } catch (\Throwable $e) {
        }
    }
    
}

if (!function_exists('______dmInit')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmInit()
    {
        ______dm('start_app', ______dmStartAppInfo());
    }
}

if (!function_exists('______dmSendFromPull')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmSendFromPull()
    {
        try {
            global $______dsn;
            global $______pull;
            if (!$______pull) {
                return;
            }
            $dsn = $______dsn;
            
            $ch = curl_init("http://receiver.demetric.172.38.128.2.nip.io/$dsn");
            try {
                $payloadArray = ______dmRecursiveScan($______pull);
                $payload = json_encode($payloadArray);
                
                if ($payload === false && $______pull !== false) {
                    throw new \Exception('payload is empty.');
                }
            } catch (\Throwable $t) {
                ob_start();
                if (class_exists(\Symfony\Component\VarDumper\VarDumper::class)) {
                    \Symfony\Component\VarDumper\VarDumper::dump($______pull);
                } else {
                    /** @noinspection ForgottenDebugOutputInspection */
                    var_dump($______pull);
                }
                $out = ob_get_clean();
                $payload = json_encode(array_merge($______pull, ['__error__' => 'json_encode failed: ' . $t->getMessage(), '__var_dump__' => $out]));
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json', 'Body-Type:pull']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 50);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_exec($ch);
            curl_close($ch);
        } catch (\Throwable $t) {
        
        }
        
        $______pull = [];
    }
}

if (!function_exists('______dmRegisterOutput')) {
    /** @noinspection PhpComposerExtensionStubsInspection */
    function ______dmRegisterOutput($output)
    {
        global $______output;
        if (isset($______output) === false) {
            $______output = null;
        }
        $______output = $output;
    }
}
