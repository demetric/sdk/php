# Example on framework: Yii 2.0

## Install
Path input point file: **common/runtime/`custom`/`demetric-sdk-php`/** - git clone this repo.  

## Include, Start app and end App log

Path input point file: **frontend/web/index.php**  
Path input point file: **frontend/web/index-test.php**  
Path input point file: **backend/web/index.php**  
Path input point file: **backend/web/index-test.php**  

Append `require(__DIR__ . '/../../common/runtime/custom/demetric-sdk-php/sdk.php');` after `require(__DIR__ . '/../../vendor/autoload.php');` .

In end file:  
- `______setDmDsn('[dsn]');`   
- `$application = new yii\web\Application($config);`   
- `______dmInit();`   
- `$application->run();`


## Codeception
Path input point file: **tests/codeception/config/frontend/functional.php**  

Append `require(YII_APP_BASE_PATH . '/common/runtime/custom/demetric-sdk-php/sdk.php');` after `require_once(YII_APP_BASE_PATH . '/vendor/autoload.php');` .

In end file:  
- `______setDmDsn('[dsn]');`   

## Error handler 

Path input point file: **vendor/yiisoft/yii2/BaseYii.php::error**


` ______dmErrorHandler($message, $category, \yii\web\HttpException::class);`
 
## DB query log

Path input point file: **vendor/yiisoft/yii2/db/Command.php::logQuery**  


`______dmDbQuery($this->getRawSql());`

## Custom log 
 
`dm([any], '[category: Default 'custom']');`


## Pull chunk/buffer size  

`______dmPullChunk(7);`    

default - 20



## Улучшенный shutdown yii2 (stream поток не будет работать пока что)
в файле **vendor/yiisoft/yii2/web/Response.php::sendContent**  
Первой строкой в методе поставьте - `        ______dmRegisterOutput($this->content);`
