Example eindex path for php:

#,id,path,alt_name,created_at,updated_at,is_hidden,sort
1,[requestId],Request Id,2023-04-01 21:05:39,2023-04-05 08:03:21,0,100
5,[mkTime],Time (microseconds),2023-04-02 12:19:14,2023-04-02 22:06:18,1,500
6,[system][http_response_code],Status code,2023-04-02 12:45:20,2023-04-02 22:04:55,0,200
7,[type],App position,2023-04-02 13:01:48,2023-04-02 22:05:40,0,300
8,[system][headers],Headers,2023-04-02 21:18:32,2023-04-02 22:40:04,1,400
9,[system][server][REQUEST_URI],Url,2023-04-02 22:43:31,2023-04-04 14:39:21,0,600
10,[system][server][DOCUMENT_URI],Path,2023-04-02 22:43:48,2023-04-04 14:03:08,1,600
11,[system][session][__id],User id,2023-04-04 10:00:46,2023-04-04 10:26:03,0,550
12,[system][headers][Location][0],Location,2023-04-04 10:28:20,2023-04-04 10:28:20,0,650
13,[system][ob_get_contents_json],Json response,2023-04-04 13:48:25,2023-04-04 13:48:25,0,650
14,[data],Custom log,2023-04-04 14:42:49,2023-04-04 14:42:49,0,660
15,[system][server][HTTP_CODECEPTION_WANT_TO],Test: Want to,2023-04-04 22:05:58,2023-04-04 22:08:35,0,660
17,[error],App error,2023-04-04 22:27:03,2023-04-04 22:27:03,0,670
18,[system][php_input_json],Json request,2023-04-05 14:39:25,2023-04-05 14:40:53,0,625
19,[system][post],Post,2023-04-05 15:55:04,2023-04-05 15:55:04,0,626
20,[query],DB Query,2023-04-06 10:41:06,2023-04-06 10:41:06,0,800

